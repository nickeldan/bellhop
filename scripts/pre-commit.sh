#!/bin/sh -ex

black --check .
mypy .
flake8 .
pytest