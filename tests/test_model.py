import enum
import io
import struct
import typing

import bellhop


def test_default_field_values() -> None:
    field = bellhop.Field()
    assert field.endian is None
    assert field.length is None
    assert field.list_length is None
    assert not field.signed
    assert not field.fallback
    assert not field.post
    assert not field.custom


def test_integer_u8() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=1)

    obj = Model(b"\x01")
    assert obj.num == 1

    obj = Model(b"\xff")
    assert obj.num == 255


def test_integer_i8() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=1, signed=True)

    obj = Model(b"\xff")
    assert obj.num == -1


def test_integer_u16_little_endian() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=2, endian=bellhop.Endian.little)

    obj = Model(b"\x01\x02")
    assert obj.num == 0x201

    obj = Model(b"\x00\x80")
    assert obj.num == 0x8000


def test_integer_u16_big_endian() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=2, endian=bellhop.Endian.big)

    obj = Model(b"\x01\x02")
    assert obj.num == 0x102

    obj = Model(b"\x80\x00")
    assert obj.num == 0x8000


def test_integer_i16() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=2, signed=True, endian=bellhop.Endian.little)

    obj = Model(b"\xff\xff")
    assert obj.num == -1


def test_integer_u32_little_endian() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=4, endian=bellhop.Endian.little)

    obj = Model(b"\x01\x02\x03\x04")
    assert obj.num == 0x4030201

    obj = Model(b"\x00\x00\x00\x80")
    assert obj.num == 0x80000000


def test_integer_u32_big_endian() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=4, endian=bellhop.Endian.big)

    obj = Model(b"\x01\x02\x03\x04")
    assert obj.num == 0x1020304

    obj = Model(b"\x80\x00\x00\x00")
    assert obj.num == 0x80000000


def test_integer_i32() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=4, signed=True, endian=bellhop.Endian.little)

    obj = Model(b"\xff\xff\xff\xff")
    assert obj.num == -1


def test_integer_u64_little_endian() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=8, endian=bellhop.Endian.little)

    obj = Model(b"\x01\x02\x03\x04\x05\x06\x07\x08")
    assert obj.num == 0x807060504030201

    obj = Model(b"\x00\x00\x00\x00\x00\x00\x00\x80")
    assert obj.num == 0x8000000000000000


def test_integer_u64_big_endian() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=8, endian=bellhop.Endian.big)

    obj = Model(b"\x01\x02\x03\x04\x05\x06\x07\x08")
    assert obj.num == 0x102030405060708

    obj = Model(b"\x80\x00\x00\x00\x00\x00\x00\x00")
    assert obj.num == 0x8000000000000000


def test_integer_i64() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=8, signed=True, endian=bellhop.Endian.little)

    obj = Model(b"\xff\xff\xff\xff\xff\xff\xff\xff")
    assert obj.num == -1


def test_integer_u32_native_endian() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=4, endian=bellhop.Endian.native)

    obj = Model(struct.pack("=I", 0x1020304))
    assert obj.num == 0x1020304


def test_bool() -> None:
    class Model(bellhop.Model):
        flag: bool

    obj = Model(b"\x00")
    assert not obj.flag

    obj = Model(b"\x01")
    assert obj.flag

    obj = Model(b"\x05")
    assert obj.flag


def test_enum() -> None:
    class Foo(enum.IntEnum):
        A = 0
        B = 1

    class Model(bellhop.Model):
        foo: Foo = bellhop.Field(length=1)

    obj = Model(b"\x00")
    assert obj.foo == Foo.A

    obj = Model(b"\x01")
    assert obj.foo == Foo.B


def test_bytes() -> None:
    class Model(bellhop.Model):
        data: bytes = bellhop.Field(length=5)

    data = bytes(range(5))
    obj = Model(data)
    assert obj.data == data

    obj = Model(bytes(range(6)))
    assert obj.data == data


def test_bytes_negative_length() -> None:
    class Model(bellhop.Model):
        data: bytes = bellhop.Field(length=-1)

    data = bytes(range(10))
    obj = Model(data)
    assert obj.data == data


def test_multiple_fields() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(length=4, endian=bellhop.Endian.big)
        flag: bool
        data: bytes = bellhop.Field(length=2)

    obj = Model(bytes(range(7)))
    assert obj.num == 0x10203
    assert obj.flag
    assert obj.data == b"\x05\x06"


def test_read_from_stream() -> None:
    class Model(bellhop.Model):
        flag: bool

    obj = Model(io.BytesIO(b"\x01"))
    assert obj.flag


def test_length_callback_for_bytes() -> None:
    user_data = object()

    class Model(bellhop.Model):
        length: int = bellhop.Field(length=1)
        data: bytes

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            assert ctx.field == "data"
            assert ctx.offset == 1
            assert ctx.user_data is user_data

            return ctx.stash["length"]

    obj = Model(b"\x03" + bytes(10), user_data=user_data)
    assert obj.data == bytes(3)


def test_stash() -> None:
    class Model(bellhop.Model):
        data1: bytes
        data2: bytes

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            if ctx.field == "data1":
                ctx.stash["foo"] = "bar"
            else:
                assert ctx.stash["foo"] == "bar"
                assert ctx.stash["data1"] == b"\x01"
                ctx.stash["data1"] = b"\xff"
            return 1

    obj = Model(b"\x01\x02")
    assert obj.data1 == b"\x01"
    assert not hasattr(obj, "foo")


def test_union() -> None:
    class Model(bellhop.Model):
        flag: bool
        field: int | bool = bellhop.Field(length=2, endian=bellhop.Endian.big)

        @classmethod
        def resolve_union(cls, ctx: bellhop.ParsingContext) -> typing.Any:
            assert ctx.field == "field"
            assert ctx.offset == 1

            return int if ctx.stash["flag"] else bool

    obj = Model(b"\x01\x01\x00")
    assert obj.field == 256

    obj = Model(b"\x00\x01\x00")
    assert isinstance(obj.field, bool)
    assert obj.field


def test_union_with_none() -> None:
    class Model(bellhop.Model):
        field: int | None = bellhop.Field(length=1)
        num: int = bellhop.Field(length=1)

        @classmethod
        def resolve_union(cls, ctx: bellhop.ParsingContext) -> typing.Any:
            return None

    obj = Model(b"\x05")
    assert obj.field is None
    assert obj.num == 5


def test_submodel() -> None:
    user_data = object()

    class Submodel(bellhop.Model):
        data: bytes

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            assert ctx.field == "data"
            assert ctx.offset == 0
            assert ctx.user_data is user_data
            assert not ctx.stash
            return 2

    class Model(bellhop.Model):
        flag1: bool
        obj: Submodel
        flag2: bool

    obj = Model(b"\x01\x01\x02\x00", user_data=user_data)
    assert obj.flag1
    assert obj.obj.data == b"\x01\x02"
    assert not obj.flag2


def test_fallback() -> None:
    class Submodel(bellhop.Model):
        data: bytes = bellhop.Field(length=4)

    class Model(bellhop.Model):
        flag: bool
        obj: Submodel | bytes = bellhop.Field(fallback=True)

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            assert ctx.field == "obj"
            assert ctx.offset == 1
            return 1

    obj = Model(b"\x00\x01\x02\x03")
    assert isinstance(obj.obj, bytes)
    assert obj.obj == b"\x01"


def test_fallback_with_bytes_length() -> None:
    class Submodel(bellhop.Model):
        data: bytes = bellhop.Field(length=4)

    class Model(bellhop.Model):
        flag: bool
        obj: Submodel | bytes = bellhop.Field(fallback=True, length=1)

    obj = Model(b"\x00\x01\x02\x03")
    assert isinstance(obj.obj, bytes)
    assert obj.obj == b"\x01"


def test_list_of_int() -> None:
    class Model(bellhop.Model):
        array: list[int] = bellhop.Field(length=1, list_length=3)

    obj = Model(bytes(range(3)))
    assert obj.array == [0, 1, 2]


def test_list_of_bytes() -> None:
    class Model(bellhop.Model):
        array: list[bytes] = bellhop.Field(length=2, list_length=3)

    obj = Model(bytes(range(6)))
    assert obj.array == [b"\x00\x01", b"\x02\x03", b"\x04\x05"]


def test_resolve_list_length() -> None:
    class Model(bellhop.Model):
        array: list[bool]

        @classmethod
        def resolve_list_length(cls, ctx: bellhop.ParsingContext) -> int:
            assert ctx.field == "array"
            assert ctx.offset == 0
            return 2

    obj = Model(b"\x00\x01")
    assert obj.array == [False, True]


def test_terminate_list() -> None:
    class Model(bellhop.Model):
        array: list[bytes] = bellhop.Field(list_length=2)

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            assert ctx.field == "array"
            assert ctx.offset == 0
            assert ctx.list_index == 0
            raise bellhop.TerminateList

    obj = Model(b"\x00")
    assert len(obj.array) == 0


def test_terminate_list_from_submodel() -> None:
    counter = 0

    class Submodel(bellhop.Model):
        flag: bool = bellhop.Field(post=True)

        @classmethod
        def post_processing(cls, ctx: bellhop.ParsingContext, value: typing.Any) -> typing.Any:
            nonlocal counter

            counter += 1
            if counter == 2:
                raise bellhop.TerminateList
            return value

    class Model(bellhop.Model):
        array: list[Submodel] = bellhop.Field(list_length=-1)

    obj = Model(b"\x00\x00")
    assert len(obj.array) == 1


def test_list_resolve_length_only_called_once() -> None:
    num_times_called = 0

    class Model(bellhop.Model):
        array: list[bytes] = bellhop.Field(list_length=2)

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            nonlocal num_times_called

            num_times_called += 1
            assert ctx.list_index == 0
            return 1

    obj = Model(b"\x00\x01\x02")
    assert obj.array == [b"\x00", b"\x01"]
    assert num_times_called == 1


def test_post_processing() -> None:
    class Model(bellhop.Model):
        flag: bool = bellhop.Field(post=True)

        @classmethod
        def post_processing(cls, ctx: bellhop.ParsingContext, value: typing.Any) -> typing.Any:
            assert ctx.field == "flag"
            assert ctx.offset == 0
            assert isinstance(value, bool)
            return not value

    obj = Model(b"\x00")
    assert obj.flag


def test_post_processing_of_list_only_called_on_list() -> None:
    class Model(bellhop.Model):
        array: list[bool] = bellhop.Field(list_length=2, post=True)

        @classmethod
        def post_processing(cls, ctx: bellhop.ParsingContext, value: typing.Any) -> typing.Any:
            assert ctx.offset == 0
            assert isinstance(value, list)
            assert len(value) == 2
            value.append(True)
            return value

    obj = Model(b"\x00\x00")
    assert obj.array == [False, False, True]


def test_configuration_set_default_endianness() -> None:
    class BigEndModel(bellhop.Model):
        __config__ = bellhop.Configuration(endian=bellhop.Endian.big)

        num: int = bellhop.Field(length=2)

    big_obj = BigEndModel(b"\x01\x02")
    assert big_obj.num == 0x102

    class LittleEndModel(bellhop.Model):
        __config__ = bellhop.Configuration(endian=bellhop.Endian.little)

        num: int = bellhop.Field(length=2)

    little_obj = LittleEndModel(b"\x01\x02")
    assert little_obj.num == 0x201


def test_custom_field() -> None:
    class Model(bellhop.Model):
        word: str = bellhop.Field(custom=True, length=3)

        @classmethod
        def resolve_custom(cls, ctx: bellhop.ParsingContext, chunk: bytes) -> typing.Any:
            assert ctx.field == "word"
            assert ctx.offset == 0
            assert chunk == b"foo"
            return "foo"

    obj = Model(b"foo")
    assert obj.word == "foo"


def test_custom_field_without_length() -> None:
    class Model(bellhop.Model):
        length: int = bellhop.Field(length=1)
        word: str = bellhop.Field(custom=True)

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            return ctx.stash["length"]

        @classmethod
        def resolve_custom(cls, ctx: bellhop.ParsingContext, chunk: bytes) -> typing.Any:
            return chunk.decode("utf-8")

    obj = Model(b"\x05hellogoodbye")
    assert obj.word == "hello"


def test_custom_basic_type() -> None:
    class Model(bellhop.Model):
        num: int = bellhop.Field(custom=True, length=3)

        @classmethod
        def resolve_custom(cls, ctx: bellhop.ParsingContext, chunk: bytes) -> typing.Any:
            return sum(chunk)

    obj = Model(b"\x01\x02\x03")
    assert obj.num == 6


def test_custom_list_callback_called_for_every_item() -> None:
    class Model(bellhop.Model):
        array: list[int] = bellhop.Field(custom=True, length=1, list_length=3, post=True)

        @classmethod
        def resolve_custom(cls, ctx: bellhop.ParsingContext, chunk: bytes) -> typing.Any:
            ctx.stash["count"] = ctx.list_index + 1
            assert len(chunk) == 1
            return chunk[0]

        @classmethod
        def post_processing(cls, ctx: bellhop.ParsingContext, value: typing.Any) -> typing.Any:
            assert ctx.stash["count"] == 3
            return value

    obj = Model(bytes(range(3)))
    assert obj.array == [0, 1, 2]


def test_implementation() -> None:
    class Foo:
        def __init__(self, data: bytes) -> None:
            self.num = data[0]

    class Model(bellhop.Model):
        __config__ = bellhop.Configuration(implementations=bellhop.Implementation(Foo, length=1))

        foo: Foo

    obj = Model(b"\x05")
    assert isinstance(obj.foo, Foo)
    assert obj.foo.num == 5


def test_implementation_with_zero_length() -> None:
    class Foo:
        def __init__(self, data: bytes) -> None:
            assert not data

    class Model(bellhop.Model):
        __config__ = bellhop.Configuration(implementations=bellhop.Implementation(Foo, length=0))

        num1: int = bellhop.Field(length=1)
        foo: Foo
        num2: int = bellhop.Field(length=1)

    obj = Model(b"\x01\x02")
    assert obj.num1 == 1
    assert isinstance(obj.foo, Foo)
    assert obj.num2 == 2


def test_implementation_with_undetermined_length() -> None:
    class Foo:
        def __init__(self, data: bytes) -> None:
            self.data = data

    class Model(bellhop.Model):
        __config__ = bellhop.Configuration(implementations=bellhop.Implementation(Foo))

        length: int = bellhop.Field(length=1)
        foo: Foo

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            assert ctx.field == "foo"
            return ctx.stash["length"]

    obj = Model(b"\x03\x01\x02\x03\x04")
    assert obj.foo.data == b"\x01\x02\x03"


def test_implementation_of_standard_type() -> None:
    class Model(bellhop.Model):
        __config__ = bellhop.Configuration(
            implementations=[
                bellhop.Implementation(int, length=3, builder=lambda x: x[0] + x[1] + x[2]),
                bellhop.Implementation(bool, length=1, builder=lambda x: not x[0]),
            ]
        )

        num: int
        flag: bool

    obj = Model(b"\x01\x02\x03\x04")
    assert obj.num == 6
    assert not obj.flag


def test_inherited_implementation() -> None:
    class ParentModel(bellhop.Model):
        __config__ = bellhop.Configuration(
            implementations=bellhop.Implementation(int, length=3, builder=lambda x: x[0] + x[1] + x[2])
        )

    class Model(ParentModel):
        __config__ = bellhop.Configuration(
            implementations=bellhop.Implementation(bool, length=1, builder=lambda x: not x[0])
        )

        num: int
        flag: bool

    obj = Model(b"\x01\x02\x03\x04")
    assert obj.num == 6
    assert not obj.flag


def test_overridden_implementation() -> None:
    class ParentModel(bellhop.Model):
        __config__ = bellhop.Configuration(
            implementations=bellhop.Implementation(int, length=3, builder=lambda x: x[0] + x[1] + x[2])
        )

    class Model(ParentModel):
        __config__ = bellhop.Configuration(
            implementations=bellhop.Implementation(int, length=1, builder=lambda x: x[0] + 1)
        )

        num: int
        data: bytes = bellhop.Field(length=-1)

    obj = Model(b"\x01\x02\x03")
    assert obj.num == 2
    assert obj.data == b"\x02\x03"


def test_custom_over_implementation() -> None:
    class Model(bellhop.Model):
        __config__ = bellhop.Configuration(
            implementations=bellhop.Implementation(int, length=1, builder=lambda x: x[0] + 1)
        )

        num: int = bellhop.Field(length=2, custom=True)

        @classmethod
        def resolve_custom(cls, ctx: bellhop.ParsingContext, chunk: bytes) -> typing.Any:
            return chunk[0] * 10 + chunk[1]

    obj = Model(b"\x01\x02")
    assert obj.num == 12


def test_list_post_processing() -> None:
    class Model(bellhop.Model):
        array: list[int] = bellhop.Field(length=1, list_length=2, list_post=True)

        @classmethod
        def list_post_processing(cls, ctx: bellhop.ParsingContext, item: int) -> int:
            assert ctx.offset == ctx.list_index
            assert item == ctx.list_index
            return item + 1

    obj = Model(bytes(range(2)))
    assert obj.array == [1, 2]
