import enum
import typing

import pytest

import bellhop


def test_invalid_annotation() -> None:
    with pytest.raises(TypeError):

        class Model(bellhop.Model):
            num: int = 5


def test_integer_missing_length() -> None:
    with pytest.raises(ValueError):

        class Model(bellhop.Model):
            num: int


def test_integer_invalid_length() -> None:
    with pytest.raises(ValueError):

        class Model(bellhop.Model):
            num: int = bellhop.Field(length=3)


def test_annotation_not_type() -> None:
    with pytest.raises(TypeError):

        class Model(bellhop.Model):
            field: 5  # type: ignore


def test_unsupported_type() -> None:
    with pytest.raises(TypeError):

        class Model(bellhop.Model):
            word: str


def test_insufficient_data() -> None:
    class Model(bellhop.Model):
        data: bytes = bellhop.Field(length=2)

    with pytest.raises(bellhop.InsufficientData) as e:
        Model(b"\x00")

    assert e.value.chain == [(Model, "data", 0)]


def test_invalid_enum() -> None:
    class Foo(enum.IntEnum):
        A = 0

    class Model(bellhop.Model):
        foo: Foo = bellhop.Field(length=1)

    with pytest.raises(bellhop.InvalidInteger) as e:
        Model(b"\x01")

    assert e.value.chain == [(Model, "foo", 0)]
    assert e.value.cls is Foo
    assert e.value.value == 1


def test_resolve_length_not_implemented() -> None:
    class Model(bellhop.Model):
        data: bytes

    with pytest.raises(NotImplementedError):
        Model(b"\x00")


def test_user_callback_error() -> None:
    class Model(bellhop.Model):
        data: bytes

        @classmethod
        def resolve_length(cls, ctx: bellhop.ParsingContext) -> int:
            1 / 0
            return 1

    with pytest.raises(bellhop.UserCallbackError) as e:
        Model(b"\x00")

    assert e.value.chain == [(Model, "data", 0)]
    assert isinstance(e.value.__cause__, ZeroDivisionError)


def test_unionized_int_must_have_length() -> None:
    with pytest.raises(ValueError):

        class Model(bellhop.Model):
            field: int | bool


def test_resolve_union_not_implemented() -> None:
    class Model(bellhop.Model):
        field: int | bool = bellhop.Field(length=1)

    with pytest.raises(NotImplementedError):
        Model(b"\x00")


def test_invalid_union_resolution() -> None:
    class Model(bellhop.Model):
        field: int | bool = bellhop.Field(length=1)

        @classmethod
        def resolve_union(cls, ctx: bellhop.ParsingContext) -> typing.Any:
            return None

    with pytest.raises(bellhop.InvalidResolution) as e:
        Model(b"\x00")

    assert e.value.chain == [(Model, "field", 0)]


def test_fallback_invalid_types() -> None:
    with pytest.raises(TypeError):

        class Model(bellhop.Model):
            field: int | None = bellhop.Field(fallback=True)

    with pytest.raises(TypeError):

        class OtherModel(bellhop.Model):
            field: int | bytes = bellhop.Field(fallback=True)


def test_fallback_missing_bytes() -> None:
    class Submodel1(bellhop.Model):
        flag: bool

    class Submodel2(bellhop.Model):
        num: int = bellhop.Field(length=2)

    with pytest.raises(TypeError):

        class Model(bellhop.Model):
            obj: Submodel1 | Submodel2 = bellhop.Field(fallback=True)


def test_submodel_error_propagated() -> None:
    class Submodel(bellhop.Model):
        flag: bool = bellhop.Field(post=True)

        @classmethod
        def post_processing(cls, ctx: bellhop.ParsingContext, value: typing.Any) -> typing.Any:
            raise Exception

    class Model(bellhop.Model):
        num: int = bellhop.Field(length=1)
        submodel: Submodel

    with pytest.raises(bellhop.UserCallbackError) as e:
        Model(b"\x00\x00")

    assert e.value.chain == [(Model, "submodel", 1), (Submodel, "flag", 0)]


def test_no_union_in_list() -> None:
    with pytest.raises(TypeError):

        class Model(bellhop.Model):
            array: list[int | bool] = bellhop.Field(length=1)


def test_resolve_list_length_not_implemented() -> None:
    class Model(bellhop.Model):
        array: list[bool]

    with pytest.raises(NotImplementedError):
        Model(b"")


def test_unhandled_terminate_list() -> None:
    class Model(bellhop.Model):
        array: list[bool]

        @classmethod
        def resolve_list_length(cls, ctx: bellhop.ParsingContext) -> int:
            raise bellhop.TerminateList

    with pytest.raises(bellhop.TerminateList) as e:
        Model(b"")

    assert e.value.chain == [(Model, "array", 0)]


def test_list_of_int_requires_length() -> None:
    with pytest.raises(ValueError):

        class Model(bellhop.Model):
            array: list[int]


def test_list_length() -> None:
    class Model(bellhop.Model):
        array: list[bool] = bellhop.Field(list_length=2)

    with pytest.raises(bellhop.InsufficientData) as e:
        Model(b"\x00")

    assert e.value.chain == [(Model, "array", 1)]


def test_post_processing_not_implemented() -> None:
    class Model(bellhop.Model):
        flag: bool = bellhop.Field(post=True)

    with pytest.raises(NotImplementedError):
        Model(b"\x00")


def test_post_processed_value_must_have_correct_type() -> None:
    class Model(bellhop.Model):
        flag: bool = bellhop.Field(post=True)

        @classmethod
        def post_processing(cls, ctx: bellhop.ParsingContext, value: typing.Any) -> typing.Any:
            return 2

    with pytest.raises(bellhop.InvalidResolution) as e:
        Model(b"\x00")

    assert e.value.chain == [(Model, "flag", 0)]


def test_post_processed_union_must_have_correct_type() -> None:
    class Model(bellhop.Model):
        field: int | bytes = bellhop.Field(length=1, post=True)

        @classmethod
        def resolve_union(cls, ctx: bellhop.ParsingContext) -> typing.Any:
            return int

        @classmethod
        def post_processing(cls, ctx: bellhop.ParsingContext, value: typing.Any) -> typing.Any:
            return False

    with pytest.raises(bellhop.InvalidResolution):
        Model(b"\x00")


def test_all_values_of_post_processed_list_must_have_correct_type() -> None:
    class Model(bellhop.Model):
        array: list[bool] = bellhop.Field(list_length=2, post=True)

        @classmethod
        def post_processing(cls, ctx: bellhop.ParsingContext, value: typing.Any) -> typing.Any:
            value[1] = 5
            return value

    with pytest.raises(bellhop.InvalidResolution):
        Model(b"\x00\x01")


def test_submodels_cannot_be_custom() -> None:
    class Submodel(bellhop.Model):
        flag: bool

    with pytest.raises(TypeError):

        class Model(bellhop.Model):
            submodel: Submodel = bellhop.Field(custom=True)


def test_resolve_custom_not_implemented() -> None:
    class Model(bellhop.Model):
        word: str = bellhop.Field(custom=True, length=3)

    with pytest.raises(NotImplementedError):
        Model(b"foo")


def test_custom_wrong_type() -> None:
    class Model(bellhop.Model):
        word: str = bellhop.Field(custom=True, length=3)

        @classmethod
        def resolve_custom(cls, ctx: bellhop.ParsingContext, chunk: bytes) -> typing.Any:
            return 5

    with pytest.raises(bellhop.InvalidResolution) as e:
        Model(b"foo")

    assert e.value.chain == [(Model, "word", 0)]


def test_implementation_wrong_type() -> None:
    def builder(data: bytes) -> typing.Any:
        return data

    class Model(bellhop.Model):
        __config__ = bellhop.Configuration(
            implementations=bellhop.Implementation(str, length=1, builder=builder)
        )

        word: str

    with pytest.raises(bellhop.InvalidResolution):
        Model(b"a")


def test_list_post_processing_not_implemented() -> None:
    class Model(bellhop.Model):
        array: list[bool] = bellhop.Field(list_length=1, list_post=True)

    with pytest.raises(NotImplementedError):
        Model(b"\x00")


def test_list_post_processing_wrong_type() -> None:
    class Model(bellhop.Model):
        array: list[bool] = bellhop.Field(list_length=1, list_post=True)

        @classmethod
        def list_post_processing(cls, ctx: bellhop.ParsingContext, item: typing.Any) -> typing.Any:
            return 0

    with pytest.raises(bellhop.InvalidResolution) as e:
        Model(b"\x00")

    assert e.value.chain == [(Model, "array", 0)]
