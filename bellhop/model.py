import collections.abc
import dataclasses
import enum
import io
import itertools
import struct
import types
import typing

from . import errors

T = typing.TypeVar("T")
P = typing.ParamSpec("P")


class Endian(enum.Enum):
    native = enum.auto()
    little = enum.auto()
    big = enum.auto()


@dataclasses.dataclass
class FieldInfo:
    endian: Endian | None
    length: int | None
    list_length: int | None
    signed: bool
    fallback: bool
    post: bool
    list_post: bool
    custom: bool


def Field(
    endian: Endian | None = None,
    length: int | None = None,
    list_length: int | None = None,
    signed: bool = False,
    fallback: bool = False,
    post: bool = False,
    list_post: bool = False,
    custom: bool = False,
) -> typing.Any:
    return FieldInfo(
        endian=endian,
        length=length,
        list_length=list_length,
        signed=signed,
        fallback=fallback,
        post=post,
        list_post=list_post,
        custom=custom,
    )


DEFAULT_FIELD_INFO: FieldInfo = Field()


def generate_fields(model: type) -> collections.abc.Iterator[tuple[str, typing.Any]]:
    for name, annotation in model.__annotations__.items():
        if typing.get_origin(annotation) is not typing.ClassVar:
            yield name, annotation


class Implementation:
    def __init__(
        self, cls: type[T], *, length: int = -1, builder: collections.abc.Callable[[bytes], T] | None = None
    ) -> None:
        self.cls = cls
        self.length = length
        self.builder: collections.abc.Callable[[bytes], T] = builder or cls


class ConfigProto(typing.Protocol):
    endian: typing.Optional[Endian]
    implementations: dict[type, Implementation]


class Configuration:
    def __init__(
        self,
        *,
        endian: Endian | None = None,
        implementations: Implementation | collections.abc.Iterable[Implementation] = (),
    ) -> None:
        self.endian = endian
        if isinstance(implementations, Implementation):
            implementations = [implementations]
        self.implementations = {implementation.cls: implementation for implementation in implementations}

    def inherit_from(self, parent: ConfigProto) -> None:
        if self.endian is None:
            self.endian = parent.endian

        for cls, implementation in parent.implementations.items():
            if cls not in self.implementations:
                self.implementations[cls] = implementation


class ModelProto(typing.Protocol):
    __config__: typing.ClassVar[Configuration]


class Verifier:
    def __init__(self, model: type[ModelProto]) -> None:
        self.model = model

    def verify(self) -> None:
        for name, annotation in generate_fields(self.model):
            self.field_info = getattr(self.model, name, DEFAULT_FIELD_INFO)
            if not isinstance(self.field_info, FieldInfo):
                raise TypeError(
                    f"Invalid value type for field {name} in class {self.model.__name__}: Expected"
                    f" bellhop.Field but got {type(self.field_info)}."
                )
            self.validate_field(name, annotation)

    def validate_field(
        self, name: str, annotation: typing.Any, in_union: bool = False, in_list: bool = False
    ) -> None:
        if (origin := typing.get_origin(annotation)) is not None:
            if origin is types.UnionType:
                if in_list:
                    raise TypeError(
                        f"Invalid type for field {name} in class {self.model.__name__}.  Cannot have a"
                        " union inside of a list."
                    )
                self.validate_union(name, annotation)
            elif origin is list:
                self.validate_field(name, annotation.__args__[0], in_list=True)
            else:
                raise TypeError(
                    f"Invalid type for field {name} in class {self.model.__name__}: {annotation}"
                )  # pragma: no cover
        elif not isinstance(annotation, type):
            raise TypeError(
                f"Field {name} in class {self.model.__name__} has invalid annotation: {annotation}"
            )
        elif self.field_info.custom:
            if issubclass(annotation, Model):
                raise TypeError(
                    f"Field {name} in class {self.model.__name__} cannot be custom because it is of a"
                    " submodel type."
                )
        elif annotation in self.model.__config__.implementations:
            pass
        elif annotation is bool:
            pass
        elif issubclass(annotation, int):
            if self.field_info.length is None:
                raise ValueError(
                    f"Missing length specification for field {name} in class {self.model.__name__}."
                )
            if self.field_info.length not in (1, 2, 4, 8):
                raise ValueError(
                    f"Invalid length specification for field {name} in class {self.model.__name__}."
                )
        elif annotation is bytes:
            pass
        elif in_union and annotation is types.NoneType:
            pass
        elif issubclass(annotation, Model) and annotation is not Model:
            pass
        else:
            raise TypeError(f"Invalid type for field {name} in class {self.model.__name__}: {annotation}")

    def validate_union(self, name: str, annotation: types.UnionType) -> None:
        if self.field_info.fallback:
            if bytes not in annotation.__args__:
                raise TypeError(
                    f"bytes must be in union when fallback is specified for field {name} in class"
                    f" {self.model.__name__}."
                )
            for subtype in annotation.__args__:
                if subtype is not bytes and not (issubclass(subtype, Model) and subtype is not Model):
                    raise TypeError(
                        f"Invalid type in union when fallback is specified for field {name} in class"
                        f" {self.model.__name__}: {subtype}"
                    )

        for subtype in annotation.__args__:
            self.validate_field(name, subtype, in_union=True)


class Reader:
    def __init__(self, data: typing.BinaryIO) -> None:
        self.data = data
        self.offset = 0
        self.previous_offset = 0

    def read(self, num: int) -> bytes:
        self.previous_offset = self.offset
        data = self.data.read(num)
        self.offset += len(data)
        return data

    def seek(self, offset: int) -> None:
        self.data.seek(offset)
        self.offset = offset
        self.previous_offset = offset


class ParsingContext:
    def __init__(self, user_data: typing.Any) -> None:
        self._user_data = user_data
        self._offset = 0
        self._field = ""
        self._list_index = 0
        self._stash: dict[str, typing.Any] = {}

    @property
    def user_data(self) -> typing.Any:
        return self._user_data

    @property
    def offset(self) -> int:
        return self._offset

    @property
    def field(self) -> str:
        return self._field

    @property
    def list_index(self) -> int:
        return self._list_index

    @property
    def stash(self) -> dict[str, typing.Any]:
        return self._stash


@dataclasses.dataclass
class ParsingCapsule:
    reader: Reader
    chain: errors.Chain
    user_data: typing.Any


class Model:
    __config__: typing.ClassVar[Configuration] = Configuration()

    def __init_subclass__(cls) -> None:
        if "__config__" in cls.__dict__:
            for parent_cls in cls.mro()[1:]:
                if (parent_config := parent_cls.__dict__.get("__config__")) is not None:
                    cls.__config__.inherit_from(parent_config)
                    break

        Verifier(cls).verify()

    def __init__(self, data: bytes | bytearray | typing.BinaryIO, user_data: typing.Any = None) -> None:
        if isinstance(user_data, ParsingCapsule):
            reader = user_data.reader
            chain = user_data.chain
            user_data = user_data.user_data
        else:
            if isinstance(data, (bytes, bytearray)):
                data = io.BytesIO(data)
            reader = Reader(data)
            chain = []

        for name, value in Parser(type(self), reader, chain, user_data).parse():
            setattr(self, name, value)

    @classmethod
    def resolve_length(cls, ctx: ParsingContext) -> int:
        raise NotImplementedError

    @classmethod
    def resolve_union(cls, ctx: ParsingContext) -> typing.Any:
        raise NotImplementedError

    @classmethod
    def resolve_list_length(cls, ctx: ParsingContext) -> int:
        raise NotImplementedError

    @classmethod
    def post_processing(cls, ctx: ParsingContext, value: typing.Any) -> typing.Any:
        raise NotImplementedError

    @classmethod
    def list_post_processing(cls, ctx: ParsingContext, item: typing.Any) -> typing.Any:
        raise NotImplementedError

    @classmethod
    def resolve_custom(cls, ctx: ParsingContext, chunk: bytes) -> typing.Any:
        raise NotImplementedError


def generate_all_fields(cls: type[Model]) -> collections.abc.Iterator[tuple[str, typing.Any]]:
    for ancestor in reversed(cls.mro()):
        if issubclass(ancestor, Model) and ancestor is not Model:
            yield from generate_fields(ancestor)


class Parser:
    def __init__(
        self, model: type[Model], reader: Reader, chain: errors.Chain, user_data: typing.Any
    ) -> None:
        self.model = model
        self.reader = reader
        self.base_offset = reader.offset
        self.chain = chain
        self.ctx = ParsingContext(user_data)
        self.field_info = DEFAULT_FIELD_INFO
        self.in_list = False
        self.list_item_length = 0
        self.list_start = 0

    def parse(self) -> collections.abc.Iterator[tuple[str, typing.Any]]:
        for field, annotation in generate_all_fields(self.model):
            self.ctx._field = field
            self.field_info = getattr(self.model, field, DEFAULT_FIELD_INFO)
            self.in_list = False

            value = self.parse_value(annotation)
            if self.field_info.post:
                value = self.handle_post_processing(value, annotation)
            self.ctx._stash[field] = value
            yield field, value

    def parse_value(self, annotation: typing.Any) -> typing.Any:
        self.ctx._offset = self.reader.offset - self.base_offset
        if (origin := typing.get_origin(annotation)) is not None:
            if origin is types.UnionType:
                return self.parse_union(annotation)
            elif origin is list:
                return self.parse_list(annotation)
            else:
                assert False  # pragma: no cover
        elif self.field_info.custom:
            return self.parse_custom(annotation)
        elif (implementation := self.model.__config__.implementations.get(annotation)) is not None:
            return self.parse_implementation(implementation)
        elif annotation is bool:
            return self.parse_bool()
        elif issubclass(annotation, int):
            return self.parse_int(annotation)
        elif annotation is bytes:
            return self.parse_bytes()
        elif issubclass(annotation, Model):
            return self.parse_submodel(annotation)
        else:
            assert False  # pragma: no cover

    def handle_post_processing(self, value: typing.Any, annotation: typing.Any) -> typing.Any:
        if self.in_list:
            self.ctx._offset = self.list_start
        value = self.callback(self.model.post_processing, value)
        if not self.is_valid_for_annotation(value, annotation):
            raise errors.InvalidResolution(
                f"Invalid type returned by post-processing.  Expected {annotation} but got {type(value)}.",
                chain=self.updated_chain(),
            )
        return value

    def is_valid_for_annotation(self, value: typing.Any, annotation: typing.Any) -> bool:
        if (origin := typing.get_origin(annotation)) is not None:
            if origin is types.UnionType:
                return any(self.is_valid_for_annotation(value, cls) for cls in annotation.__args__)
            elif origin is list:
                return isinstance(value, list) and all(
                    isinstance(item, annotation.__args__[0]) for item in value
                )
            else:
                assert False  # pragma: no cover

        assert isinstance(annotation, type)
        if not isinstance(value, annotation):
            return False

        if isinstance(value, bool) and annotation is not bool:
            return False

        return True

    def updated_chain(self) -> errors.Chain:
        return self.chain + [(self.model, self.ctx._field, self.ctx._offset)]

    def callback(
        self,
        func: collections.abc.Callable[typing.Concatenate[ParsingContext, P], T],
        *args: P.args,
        **kwargs: P.kwargs,
    ) -> T:
        try:
            return func(self.ctx, *args, **kwargs)
        except (NotImplementedError, AssertionError):
            raise
        except errors.TerminateList as e:
            e.chain = self.updated_chain()
            raise
        except Exception as e:
            raise errors.UserCallbackError(chain=self.updated_chain()) from e

    def field_length(self) -> int:
        if self.in_list and self.list_item_length > 0:
            return self.list_item_length

        length = (
            self.field_info.length
            if self.field_info.length is not None
            else self.callback(self.model.resolve_length)
        )
        if self.in_list:
            self.list_item_length = length
        return length

    def get_data(self, num: int) -> bytes:
        try:
            data = self.reader.read(num)
        except Exception as e:
            raise errors.ReadError(chain=self.updated_chain()) from e
        if len(data) < num:
            raise errors.InsufficientData(
                f"Insufficient data for field {self.ctx._field} in class {self.model.__name__}",
                chain=self.updated_chain(),
            )
        return data

    def parse_custom(self, annotation: type) -> typing.Any:
        chunk = self.parse_bytes()
        value = self.callback(self.model.resolve_custom, chunk)
        if not isinstance(value, annotation):
            raise errors.InvalidResolution(
                f"Invalid type for field {self.ctx._field} in class {self.model.__name__}.  Expected"
                f" {annotation} but got {type(value)}.",
                chain=self.updated_chain(),
            )
        return value

    def parse_implementation(self, implementation: Implementation) -> typing.Any:
        length = implementation.length if implementation.length >= 0 else self.field_length()
        chunk = self.get_data(length)
        obj = implementation.builder(chunk)
        if not isinstance(obj, implementation.cls):
            raise errors.InvalidResolution(
                f"Invalid type for field {self.ctx._field} in class {self.model.__name__}.  Expected"
                f" {implementation.cls} but got {type(obj)}.",
                chain=self.updated_chain(),
            )
        return obj

    def parse_bool(self) -> bool:
        return bool(self.get_data(1)[0])

    def parse_int(self, annotation: type[int]) -> int:
        assert self.field_info.length is not None

        fmt: str
        endian = self.field_info.endian or self.model.__config__.endian or Endian.native
        match endian:
            case Endian.native:
                fmt = "="
            case Endian.little:
                fmt = "<"
            case Endian.big:
                fmt = ">"

        match self.field_info.length:
            case 1:
                fmt += "B"
            case 2:
                fmt += "H"
            case 4:
                fmt += "I"
            case 8:
                fmt += "Q"

        if self.field_info.signed:
            fmt = fmt.lower()

        num = struct.unpack(fmt, self.get_data(self.field_info.length))[0]
        try:
            return annotation(num)
        except ValueError as e:
            raise errors.InvalidInteger(
                f"Invalid value for class {annotation.__name__}: {num}",
                cls=annotation,
                value=num,
                chain=self.updated_chain(),
            ) from e

    def parse_bytes(self) -> bytes:
        return self.get_data(self.field_length())

    def parse_submodel(self, submodel: type[Model]) -> typing.Any:
        current_offset = self.reader.offset
        failed = False
        try:
            obj = submodel(
                b"",
                ParsingCapsule(reader=self.reader, chain=self.updated_chain(), user_data=self.ctx.user_data),
            )
        except errors.TerminateList:
            raise
        except errors.Error:
            if not self.field_info.fallback:
                raise
            failed = True
        if failed:
            self.reader.seek(current_offset)
            return self.parse_value(bytes)
        return obj

    def parse_union(self, annotation: types.UnionType) -> typing.Any:
        if self.field_info.fallback and len(annotation.__args__) == 2:
            for submodel in annotation.__args__:
                if submodel is not bytes:
                    break
            return self.parse_value(submodel)

        cls = self.callback(self.model.resolve_union)
        if cls is None:
            cls = types.NoneType
        if cls not in annotation.__args__:
            raise errors.InvalidResolution(
                f"Unexpected type returned from resolve_union.  Expected one of {annotation.__args__} but got"
                f" {cls}.",
                chain=self.updated_chain(),
            )

        return None if cls is types.NoneType else self.parse_value(cls)

    def parse_list(self, annotation: types.GenericAlias) -> list:
        self.in_list = True
        self.list_item_length = 0
        self.list_start = self.reader.offset - self.base_offset

        length = (
            self.field_info.list_length
            if self.field_info.list_length is not None
            else self.callback(self.model.resolve_list_length)
        )
        iterable: collections.abc.Iterable[int]
        if length < 0:
            iterable = itertools.count(0)
        else:
            iterable = range(length)

        item_type = annotation.__args__[0]
        value: list[typing.Any] = []

        try:
            for k in iterable:
                self.ctx._list_index = k
                item = self.parse_value(item_type)
                if self.field_info.list_post:
                    item = self.callback(self.model.list_post_processing, item)
                    if not isinstance(item, item_type):
                        raise errors.InvalidResolution(
                            f"Unexpected type returned from list_post_processing.  Expected {item_type} but"
                            f" got {type(item)}.",
                            chain=self.updated_chain(),
                        )
                value.append(item)
        except errors.TerminateList:
            pass

        return value
